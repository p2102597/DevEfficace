#include<iostream>
#include<fstream>
#include<random>

#include"Complexite.h"

using namespace std;

int const N = 100;


void remplirTab1D(int tab[], int nbr,Complexite* c) {
	
	for (size_t i = 0; i < nbr; i++)
	{
		c->addBoucle();
		c->addTest(); // i < nbr
		c->addOperateur();c->addOperateur();// i++
		tab[i] = rand();
		c->addOperateur();// tab[i]=rand();
	}
}

void remplirTab2D(int tab[][N], int nbr, Complexite* c) {
	
	for (size_t i = 0; i < nbr; i++)
	{
		c->addTest();//i < nbr;
		c->addOperateur();c->addOperateur();//i++
		for (size_t y = 0; y < nbr; y++)
		{
			c->addBoucle();
			c->addTest();//y < nbr
			c->addOperateur();c->addOperateur();//y++
			tab[i][y] = rand();
			c->addOperateur(); // tab[i]=rand();
		}
	}
}
void remplirMaticeOpti(int tab[][N], int nbr, Complexite* c) {
	for (size_t i = 0; i < nbr; i++)
	{
		c->addBoucle();
		c->addTest(); // i < nbr
		c->addOperateur(); c->addOperateur();// i++
		tab[i][i] = rand();
		c->addOperateur();// tab[i]=rand();
	}
}

void remplirMaticeNonOpti(int tab[][N], int nbr, Complexite* c) {

	for (size_t i = 0; i < nbr; i++)
	{
		c->addTest();//i<nbr;
		c->addOperateur(); c->addOperateur();//i++
		for (size_t y = 0; y < nbr; y++)
		{
			c->addBoucle();
			c->addTest();//y < nbr
			c->addOperateur(); c->addOperateur();//y++
			c->addTest();//i==y
			if (i == y) {
				tab[i][y] = rand();
				c->addOperateur(); // tab[i]=rand();
			}
		}
	}
}

int factoriel(int f, Complexite* c) {
	c->addTest();
	if (f > 1) {
		c->addOperateur();
		c->addOperateur();
		c->addAppelFonction();
		return f * factoriel(f - 1, c);
	}
	else {
		return 1;
	}
}
int factorielnoRecustife(int f, Complexite* c) {
	int tmp = 1;
	c->addOperateur();
	for (size_t i = 1; i < f; i++)
	{
		c->addBoucle();
		c->addOperateur(); c->addOperateur(); c->addTest();
		c->addOperateur(); c->addOperateur();
		tmp = tmp * 1;
	}
	return tmp;
}

void triBulle(int Tab[], int nbr) {

}


void main() {
	srand(time(NULL));

	Complexite* tab1D = new Complexite();
	tab1D->init();
	int tab1[N];
	remplirTab1D(tab1, N, tab1D);
	tab1D->LoadCSV("tab1D");

	Complexite* tab2D = new Complexite();
	tab2D->init();
	int tab2[N][N];
	remplirTab2D(tab2, N, tab2D);
	tab2D->LoadCSV("tab2D");

	Complexite* matriceOpti = new Complexite();
	matriceOpti->init();
	int matcice1[N][N];
	remplirMaticeOpti(matcice1, N, matriceOpti);
	matriceOpti->LoadCSV("matcice1");

	Complexite* matriceNonOpti = new Complexite();
	matriceNonOpti->init();
	int matcice2[N][N];
	remplirMaticeNonOpti(matcice2, N, matriceNonOpti);
	matriceNonOpti->LoadCSV("matcice2");

	Complexite* facto = new Complexite();
	facto->init();
	factoriel(N, facto);
	facto->LoadCSV("facto1");

	Complexite* facto2 = new Complexite();
	facto2->init();
	factorielnoRecustife(N, facto2);
	facto2->LoadCSV("facto2");


}