#include "Complexite.h"

void Complexite::init()
{
	Nb_boucles=0;
	Nb_operation_unitaires=0;
	Nb_appel_fonctions=0;
	Nb_tests=0;
	TimeStart=clock();
}

void Complexite::addBoucle()
{
	Nb_boucles++;
}

void Complexite::addOperateur()
{
	Nb_operation_unitaires++;
}

void Complexite::addAppelFonction()
{
	Nb_appel_fonctions++;
}

void Complexite::addTest()
{
	Nb_tests++;
}

void Complexite::LoadCSV(string s) {
	std::ofstream fileD(s+".csv");
	if (fileD.is_open())
	{
		fileD << string("Nb_boucles;Nb_operation_unitaires;Nb_appel_fonctions;Nb_tests;Time\n");
		fileD << to_string(Nb_boucles) + ';'+ to_string(Nb_operation_unitaires)+';'+ to_string(Nb_appel_fonctions) +';'+ to_string(Nb_tests)+';'+ to_string(clock() - TimeStart) + '\n';
	}
}
