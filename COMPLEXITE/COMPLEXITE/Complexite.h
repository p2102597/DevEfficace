#pragma once

#include<iostream>
#include<fstream>
#include<string>

using namespace std;

class Complexite
{
private:
	int Nb_boucles;
	int Nb_operation_unitaires;
	int Nb_appel_fonctions;
	int Nb_tests;
	clock_t TimeStart;
	
public:
	void init();
	void LoadCSV(string);
	void addBoucle();
	void addOperateur();
	void addAppelFonction();
	void addTest();
};

