#include "TreeList.h"

#include<iostream>
using namespace std;


void TreeList::deleteAll(Node* n)
{
	if (n != nullptr) {
		for (auto c : n->childs) {
			deleteAll(c);
		}
		delete(n);
	}
}

string TreeList::afficheAll(Node* n,int cpt) const
{
	string s;
	if (n != nullptr) {
		
		s+= printNode(n, cpt)+="\n";
		cpt++;
		if (n->status) {
			for (auto c : n->childs) {
				s += afficheAll(c, cpt);
			}
		}
	}
	return s;
}

string TreeList::printNode(Node* n, int l) const
{
	string s = "";
	for (size_t i = 0; i < l; i++)
	{
		s += "\t";
	}
	if (n == current) {
		if(n->status) s += "v";
		else s += ">";
		
	}
	else if (n->status) {
		s += "o";
	}
	else {
		s += "x";
	}
	s += " "+n->name;
	return s;
}

bool TreeList::searchAll(Node* n, string name)
{
	if (n != nullptr) {
		bool b=false;
		vector<bool> table;

		for (auto c : n->childs) {
			b=searchAll(c,name);
			table.push_back(b);
		}
		for (auto b : table) {
			if (b ) {
				n->status = true;
				return true;
			}
		}
		if (n->name.find(name) != string::npos) {
			n->status = true;
			return true;
		}
		else {
			n->status = false;
			return false;
		}
		
		
	}
}

TreeList::TreeList()
{
	racine = nullptr;
	current = nullptr;
}

TreeList::~TreeList()
{
	current = nullptr;
	deleteAll(racine);
	racine = nullptr;
}

void TreeList::addChild(string name)
{
	Node* node = new Node();
	node->name = name;
	if (racine==nullptr)
	{
		node->parent = nullptr;
		node->status = true;
		current = node;
		racine = node;
	}
	else {
		node->parent = current;
		node->status = true;
		current->childs.push_back(node);
	}
}

void TreeList::resetCurrentPos()
{
	current = racine;
}

bool TreeList::moveToChild(int index)
{
	if (current == nullptr)return false;
	if (index >= current->childs.size()) return false;
	current = current->childs.at(index);
	current->status = true;
	return true;
	
}

bool TreeList::moveToParent()
{
	if(current==nullptr)return false;
	if(current->parent==nullptr) return false;
	current = current->parent;
	
}

void TreeList::remove(Node* n)
{
	if (n != nullptr) {
		for (auto c : n->childs) {
			deleteAll(c);
		}

		int i = 0;
		for (auto node : n->parent->childs) {
			if (node == n) {
				n->parent->childs.erase(current->childs.begin() + i);
			}
			i++;
		}
		delete(n);
		
	}
}

void TreeList::removeAllChild()
{
	if (current != nullptr) {
		for (auto c : current->childs) {
			deleteAll(c);
		}
		current->childs.clear();
	}
}

void TreeList::removeChild(int i)
{
	if (current != nullptr) {
		if (current->childs.size() < i)
			remove(current->childs.at(i));
	}
}

void TreeList::removeChild(string s)
{
	if (current != nullptr) {
		int cpt = 0;
		for (auto node : current->childs) {
			if (node->name == s) {
				remove(node);
			}
		}
	}
}

void TreeList::editName(string name)
{
	if (current != nullptr) {
		current->name = name;
	}
}

void TreeList::changeStatues()
{
	if (current != nullptr) {
		current->status = !current->status;
	}
}

void TreeList::search(string name)
{
	searchAll(racine, name);
}

ostream& operator<<(ostream& os, const TreeList& dt)
{
	int cpt = 0;
	os << dt.afficheAll(dt.racine, cpt);
	return os;
}
