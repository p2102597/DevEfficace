#pragma once
#include "Node.h"

#include<iostream>
using namespace std;

class TreeList
{
private:
	Node* racine;
	Node* current;
	void deleteAll(Node*);
	string afficheAll(Node*,int) const;
	string printNode(Node* n, int l) const;
	bool searchAll(Node* n, string name);

public:
	TreeList();
	~TreeList();

	void addChild(string name);
	void resetCurrentPos();
	bool moveToChild(int index);
	bool moveToParent();

	void remove(Node*);
	void removeAllChild();
	void removeChild(int);
	void removeChild(string);

	void editName(string name);
	void changeStatues();

	void search(string name);

	
	friend ostream& operator<<(ostream & os, const TreeList & dt);

};

